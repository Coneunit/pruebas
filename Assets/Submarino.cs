﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Submarino : MonoBehaviour
{

    // Use this for initialization
    public float velocidadDespzamiento = 0.01f;
    public int vidas = 3;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            // float transformH = Input.GetAxis("Horizontal") * velocidadDespzamiento * Time.deltaTime;
            transform.Translate(-Vector2.right * Time.deltaTime * velocidadDespzamiento);
        }

        if (Input.GetKey(KeyCode.D))
        {
            // float transformH = Input.GetAxis("Horizontal") * velocidadDespzamiento * Time.deltaTime;
            transform.Translate(Vector2.right * Time.deltaTime * velocidadDespzamiento);
        }
        if (Input.GetKey(KeyCode.W))
        {
            // float transformH = Input.GetAxis("Horizontal") * velocidadDespzamiento * Time.deltaTime;
            transform.Translate(Vector2.up * Time.deltaTime * velocidadDespzamiento);
        }

        if (Input.GetKey(KeyCode.S))
        {
            // float transformH = Input.GetAxis("Horizontal") * velocidadDespzamiento * Time.deltaTime;
            transform.Translate(Vector2.down * Time.deltaTime * velocidadDespzamiento);
        }
    }


    void invokeBlinkBarco()
    {
        InvokeRepeating("IniciarBlinkBarco", 0, 0.4f);
    }

    void IniciarBlinkBarco()
    {
        StartCoroutine(BlinkBarco());
    }

    IEnumerator BlinkBarco()
    {
        this.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(0.2f);
        this.GetComponent<Renderer>().enabled = true;
    }
}
